***
Git is a trademark of Software Freedom Conservancy and our use of 'GitLab' is under license  
View [page source](https://gitlab.com/gitlab-org/professional-services-automation/pipelinecoe/pipeline-templates/-/tree/main/documentation) — Edit in [Web IDE](https://gitlab.com/-/ide/project/gitlab-org/professional-services-automation/pipelinecoe/pipeline-templates/edit/main/-/documentation/) — please [contribute](https://gitlab-org.gitlab.io/professional-services-automation/pipelinecoe/pipeline-templates/#/Contributor)  
&copy; 2022 GitLab B.V.
