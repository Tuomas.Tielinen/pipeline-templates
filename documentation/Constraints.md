# Introduction

Usage of the Pipeline COE Pattern and style of development comes with some constraints. There are things you can and cannot do while using this pattern. We'll try to list each of these and be upfront about them. But we can't anticipate every customer's needs, so you may have a situation or pattern the Pipeline COE is incompatible with.